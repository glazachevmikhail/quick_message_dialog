import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:quick_message_dialog/bloc/auth/auth_bloc.dart';
import 'package:quick_message_dialog/bloc/quick_responses/quick_responses_bloc.dart';
import 'package:quick_message_dialog/widgets/quick_responses/quick_responses_screen_widget.dart';

import 'widgets/auth_screen/auth_screen_widget.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => BlocProvider(
              create: (_) => AuthBloc(),
              child: const AuthScreenWidget(),
            ),
        '/quick_responses': (context) => BlocProvider(
            create: (_) => QuickResponsesBloc(),
            child: const QuickResponsesScreenWidget()),
      },
    );
  }
}
