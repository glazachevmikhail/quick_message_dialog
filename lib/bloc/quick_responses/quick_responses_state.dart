import 'package:quick_message_dialog/models/auth/auth_response.dart';

import '../../models/quick_responses/quick_responses.dart';

abstract class QuickResponsesState {}

class QuickResponsesEmptyState extends QuickResponsesState {}

class QuickResponsesLoadingState extends QuickResponsesState {}

class QuickResponsesLoadedState extends QuickResponsesState {
  final List<QuickResponsesData> quickResponsesList;

  QuickResponsesLoadedState({
    required this.quickResponsesList,
  });
}

class QuickResponsesErrorState extends QuickResponsesState {}
