import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quick_message_dialog/bloc/quick_responses/quick_responses_event.dart';
import 'package:quick_message_dialog/bloc/quick_responses/quick_responses_state.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../models/quick_responses/quick_responses.dart';
import '../../services/api_service.dart';

class QuickResponsesBloc
    extends Bloc<QuickResponsesEvent, QuickResponsesState> {
  void tokenSaved() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(prefs.getString('auth_token'));
    assesToken = prefs.getString('auth_token') ?? '';
  }

  String assesToken = '';
  String query = '';
  final ApiService _apiService = ApiService(Dio());

  Future<List<QuickResponsesData>> search() async {
    try {
      final quickResponses =
          await _apiService.getQuickResponses(token: assesToken, query: query);
      final response = quickResponses.data;

      if (response.success) {
        final data = response.data;
        return data;
      } else {
        throw Exception('error: ${quickResponses.response.statusCode}');
      }
    } on DioError catch (e) {
      throw Exception('ERROR: $e');
    }
  }

  QuickResponsesBloc() : super(QuickResponsesEmptyState()) {
    tokenSaved();
    on<QuickResponsesSearchEvent>((event, emit) async {
      emit(QuickResponsesLoadingState());

      try {
        final List<QuickResponsesData> quickResponsesList = await search();
        emit(QuickResponsesLoadedState(quickResponsesList: quickResponsesList));
      } catch (_) {
        emit(QuickResponsesErrorState());
      }
    });
    on<QuickResponsesSearchQueryEmptyEvent>((event, emit) {
      emit(QuickResponsesEmptyState());
    });
  }
}
