abstract class QuickResponsesEvent {}

class QuickResponsesSearchEvent extends QuickResponsesEvent {}

class QuickResponsesSearchQueryEmptyEvent extends QuickResponsesEvent {}
