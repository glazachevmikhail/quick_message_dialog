import 'package:quick_message_dialog/models/auth/auth_response.dart';

abstract class AuthState {}

class AuthEmptyState extends AuthState {}

class AuthLoadingState extends AuthState {}

class AuthLoadedState extends AuthState {
  final AuthResponseData? authResponseData;

  AuthLoadedState({
    required this.authResponseData,
  });
}

class AuthErrorState extends AuthState {}
