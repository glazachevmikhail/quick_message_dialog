import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quick_message_dialog/bloc/auth/auth_event.dart';
import 'package:quick_message_dialog/bloc/auth/auth_state.dart';
import 'package:quick_message_dialog/models/auth/auth_response.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../models/auth/auth_request.dart';
import '../../services/api_service.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final ApiService authService = ApiService(Dio());
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  String token = '';

  void showQuickResponsesScreen(BuildContext context) {
    Navigator.of(context).pushReplacementNamed('/quick_responses');
  }

  Future<AuthResponseData?> login() async {
    final String email = emailController.text;
    final String password = passwordController.text;
    const String appId = 'webchat';
    final AuthRequest authData =
        AuthRequest(password: password, email: email, appId: appId);

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      final ResponseData<AuthResponseData> authResponse =
          await authService.login(authData);
      if (authResponse.success) {
        final data = authResponse.data;

        final token = data.accessToken;
        final date = data.accessTokenEndTime;
        await prefs.setString('auth_token', token);
        await prefs.setInt('date', date);
        print('tokensaved = ${prefs.getString('auth_token')}');
        print('Token: $token');
        return data;
      } else {
        print('wrong login or password');
      }
    } on DioError catch (e) {
      print('ERROR $e');
    }
  }

  AuthBloc() : super(AuthEmptyState()) {
    on<AuthLoadEvent>((event, emit) async {
      emit(AuthLoadingState());

      try {
        final AuthResponseData? authResponseData = await login();
        emit(AuthLoadedState(authResponseData: authResponseData));
      } catch (_) {
        emit(AuthErrorState());
      }
    });
  }
}
