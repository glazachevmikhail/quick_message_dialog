import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_acrylic/flutter_acrylic.dart';
import 'package:global_shortcuts/global_shortcuts.dart';
import 'package:keyboard_event/keyboard_event.dart';
import 'package:quick_message_dialog/my_app.dart';
import 'package:window_manager/window_manager.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Window.initialize();
  if (Platform.isWindows) {
    await Window.hideWindowControls();
    await Window.setEffect(effect: WindowEffect.mica);
    Window.makeWindowFullyTransparent();
  }
  // await register();
  // initPlatformState();
  runApp(const MyApp());
  await WindowManager.instance.ensureInitialized();
  windowManager.waitUntilReadyToShow().then((_) async {});
}

/// MAC OS
Future<void> register() async {
  try {
    await GlobalShortcuts.register(
      key: ShortcutKey.r,
      modifiers: [ShortcutModifier.control],
      onKeyCombo: _onKeyCombo,
    );
  } on Exception catch (_) {}
}

/// WINDOWS
KeyboardEvent keyboardEvent = KeyboardEvent();

int lastBTM = 0;
Future<void> initPlatformState() async {
  keyboardEvent.startListening((keyEvent) {
    if (keyEvent.isKeyDown) {
      if (keyEvent.vkCode == 27) {
        windowManager.minimize();
      }
      if (lastBTM != keyEvent.vkCode) {
        if ((keyEvent.vkCode == 32 && lastBTM == 160) ||
            (keyEvent.vkCode == 160 && lastBTM == 32)) {
          windowManager.focus();
          lastBTM = 0;
          return;
        }
        lastBTM = keyEvent.vkCode;
      }
    }
    if (keyEvent.isKeyUP) {
      lastBTM = 0;
    }
  });
}

void _onKeyCombo() => print('Shortcut Pressed at ${DateTime.now()}');
