import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_response.g.dart';
part 'auth_response.freezed.dart';

@JsonSerializable(genericArgumentFactories: true)
class ResponseData<T> {
  final bool success;
  final T data;

  ResponseData(this.success, this.data);

  factory ResponseData.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$ResponseDataFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T value) toJsonT) =>
      _$ResponseDataToJson<T>(this, toJsonT);
}

@freezed
class AuthResponseData with _$AuthResponseData {
  factory AuthResponseData({
    required int cabinetUserId,
    required String accessToken,
    required int accessTokenEndTime,
    required String refreshToken,
    required int refreshTokenEndTime,
    required bool cabinetUserIsAdmin,
  }) = _AuthResponseData;

  factory AuthResponseData.fromJson(Map<String, dynamic> json) =>
      _$AuthResponseDataFromJson(json);
}
