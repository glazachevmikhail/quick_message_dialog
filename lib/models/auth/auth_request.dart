import 'package:freezed_annotation/freezed_annotation.dart';
part 'auth_request.g.dart';
part 'auth_request.freezed.dart';

@freezed
class AuthRequest with _$AuthRequest {
  factory AuthRequest({
    required String email,
    required String password,
    @Default("webchat") String appId,
  }) = _AuthRequest;

  factory AuthRequest.fromJson(Map<String, dynamic> json) =>
      _$AuthRequestFromJson(json);
}
