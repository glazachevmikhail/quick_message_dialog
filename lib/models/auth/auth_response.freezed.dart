// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'auth_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AuthResponseData _$AuthResponseDataFromJson(Map<String, dynamic> json) {
  return _AuthResponseData.fromJson(json);
}

/// @nodoc
mixin _$AuthResponseData {
  int get cabinetUserId => throw _privateConstructorUsedError;
  String get accessToken => throw _privateConstructorUsedError;
  int get accessTokenEndTime => throw _privateConstructorUsedError;
  String get refreshToken => throw _privateConstructorUsedError;
  int get refreshTokenEndTime => throw _privateConstructorUsedError;
  bool get cabinetUserIsAdmin => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthResponseDataCopyWith<AuthResponseData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthResponseDataCopyWith<$Res> {
  factory $AuthResponseDataCopyWith(
          AuthResponseData value, $Res Function(AuthResponseData) then) =
      _$AuthResponseDataCopyWithImpl<$Res, AuthResponseData>;
  @useResult
  $Res call(
      {int cabinetUserId,
      String accessToken,
      int accessTokenEndTime,
      String refreshToken,
      int refreshTokenEndTime,
      bool cabinetUserIsAdmin});
}

/// @nodoc
class _$AuthResponseDataCopyWithImpl<$Res, $Val extends AuthResponseData>
    implements $AuthResponseDataCopyWith<$Res> {
  _$AuthResponseDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? cabinetUserId = null,
    Object? accessToken = null,
    Object? accessTokenEndTime = null,
    Object? refreshToken = null,
    Object? refreshTokenEndTime = null,
    Object? cabinetUserIsAdmin = null,
  }) {
    return _then(_value.copyWith(
      cabinetUserId: null == cabinetUserId
          ? _value.cabinetUserId
          : cabinetUserId // ignore: cast_nullable_to_non_nullable
              as int,
      accessToken: null == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String,
      accessTokenEndTime: null == accessTokenEndTime
          ? _value.accessTokenEndTime
          : accessTokenEndTime // ignore: cast_nullable_to_non_nullable
              as int,
      refreshToken: null == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
      refreshTokenEndTime: null == refreshTokenEndTime
          ? _value.refreshTokenEndTime
          : refreshTokenEndTime // ignore: cast_nullable_to_non_nullable
              as int,
      cabinetUserIsAdmin: null == cabinetUserIsAdmin
          ? _value.cabinetUserIsAdmin
          : cabinetUserIsAdmin // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AuthResponseDataCopyWith<$Res>
    implements $AuthResponseDataCopyWith<$Res> {
  factory _$$_AuthResponseDataCopyWith(
          _$_AuthResponseData value, $Res Function(_$_AuthResponseData) then) =
      __$$_AuthResponseDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int cabinetUserId,
      String accessToken,
      int accessTokenEndTime,
      String refreshToken,
      int refreshTokenEndTime,
      bool cabinetUserIsAdmin});
}

/// @nodoc
class __$$_AuthResponseDataCopyWithImpl<$Res>
    extends _$AuthResponseDataCopyWithImpl<$Res, _$_AuthResponseData>
    implements _$$_AuthResponseDataCopyWith<$Res> {
  __$$_AuthResponseDataCopyWithImpl(
      _$_AuthResponseData _value, $Res Function(_$_AuthResponseData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? cabinetUserId = null,
    Object? accessToken = null,
    Object? accessTokenEndTime = null,
    Object? refreshToken = null,
    Object? refreshTokenEndTime = null,
    Object? cabinetUserIsAdmin = null,
  }) {
    return _then(_$_AuthResponseData(
      cabinetUserId: null == cabinetUserId
          ? _value.cabinetUserId
          : cabinetUserId // ignore: cast_nullable_to_non_nullable
              as int,
      accessToken: null == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String,
      accessTokenEndTime: null == accessTokenEndTime
          ? _value.accessTokenEndTime
          : accessTokenEndTime // ignore: cast_nullable_to_non_nullable
              as int,
      refreshToken: null == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
      refreshTokenEndTime: null == refreshTokenEndTime
          ? _value.refreshTokenEndTime
          : refreshTokenEndTime // ignore: cast_nullable_to_non_nullable
              as int,
      cabinetUserIsAdmin: null == cabinetUserIsAdmin
          ? _value.cabinetUserIsAdmin
          : cabinetUserIsAdmin // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AuthResponseData implements _AuthResponseData {
  _$_AuthResponseData(
      {required this.cabinetUserId,
      required this.accessToken,
      required this.accessTokenEndTime,
      required this.refreshToken,
      required this.refreshTokenEndTime,
      required this.cabinetUserIsAdmin});

  factory _$_AuthResponseData.fromJson(Map<String, dynamic> json) =>
      _$$_AuthResponseDataFromJson(json);

  @override
  final int cabinetUserId;
  @override
  final String accessToken;
  @override
  final int accessTokenEndTime;
  @override
  final String refreshToken;
  @override
  final int refreshTokenEndTime;
  @override
  final bool cabinetUserIsAdmin;

  @override
  String toString() {
    return 'AuthResponseData(cabinetUserId: $cabinetUserId, accessToken: $accessToken, accessTokenEndTime: $accessTokenEndTime, refreshToken: $refreshToken, refreshTokenEndTime: $refreshTokenEndTime, cabinetUserIsAdmin: $cabinetUserIsAdmin)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AuthResponseData &&
            (identical(other.cabinetUserId, cabinetUserId) ||
                other.cabinetUserId == cabinetUserId) &&
            (identical(other.accessToken, accessToken) ||
                other.accessToken == accessToken) &&
            (identical(other.accessTokenEndTime, accessTokenEndTime) ||
                other.accessTokenEndTime == accessTokenEndTime) &&
            (identical(other.refreshToken, refreshToken) ||
                other.refreshToken == refreshToken) &&
            (identical(other.refreshTokenEndTime, refreshTokenEndTime) ||
                other.refreshTokenEndTime == refreshTokenEndTime) &&
            (identical(other.cabinetUserIsAdmin, cabinetUserIsAdmin) ||
                other.cabinetUserIsAdmin == cabinetUserIsAdmin));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      cabinetUserId,
      accessToken,
      accessTokenEndTime,
      refreshToken,
      refreshTokenEndTime,
      cabinetUserIsAdmin);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AuthResponseDataCopyWith<_$_AuthResponseData> get copyWith =>
      __$$_AuthResponseDataCopyWithImpl<_$_AuthResponseData>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AuthResponseDataToJson(
      this,
    );
  }
}

abstract class _AuthResponseData implements AuthResponseData {
  factory _AuthResponseData(
      {required final int cabinetUserId,
      required final String accessToken,
      required final int accessTokenEndTime,
      required final String refreshToken,
      required final int refreshTokenEndTime,
      required final bool cabinetUserIsAdmin}) = _$_AuthResponseData;

  factory _AuthResponseData.fromJson(Map<String, dynamic> json) =
      _$_AuthResponseData.fromJson;

  @override
  int get cabinetUserId;
  @override
  String get accessToken;
  @override
  int get accessTokenEndTime;
  @override
  String get refreshToken;
  @override
  int get refreshTokenEndTime;
  @override
  bool get cabinetUserIsAdmin;
  @override
  @JsonKey(ignore: true)
  _$$_AuthResponseDataCopyWith<_$_AuthResponseData> get copyWith =>
      throw _privateConstructorUsedError;
}
