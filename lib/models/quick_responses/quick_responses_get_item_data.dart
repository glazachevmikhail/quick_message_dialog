import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:quick_message_dialog/models/quick_responses/quick_responses.dart';
part 'quick_responses_get_item_data.freezed.dart';
part 'quick_responses_get_item_data.g.dart';

@freezed
class QuickResponsesGetItemData with _$QuickResponsesGetItemData {
  factory QuickResponsesGetItemData({
    required int? id,
    required String? name,
    required int? sort,
    required Parent? parent,
    required String? text,
    required String? keywords,
    required bool? enableAI,
  }) = _QuickResponsesGetItemData;

  factory QuickResponsesGetItemData.fromJson(Map<String, dynamic> json) =>
      _$QuickResponsesGetItemDataFromJson(json);
}
