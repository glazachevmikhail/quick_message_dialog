import 'package:freezed_annotation/freezed_annotation.dart';

part 'quick_responses.freezed.dart';
part 'quick_responses.g.dart';

@freezed
class QuickResponsesData with _$QuickResponsesData {
  factory QuickResponsesData({
    required int id,
    required String name,
    required int sort,
    required ListData list,
    required Parent? parent,
  }) = _QuickResponsesData;

  factory QuickResponsesData.fromJson(Map<String, dynamic> json) =>
      _$QuickResponsesDataFromJson(json);
}

@freezed
class ListData with _$ListData {
  factory ListData({
    required int id,
    required String name,
  }) = _ListData;

  factory ListData.fromJson(Map<String, dynamic> json) =>
      _$ListDataFromJson(json);
}

@freezed
class Parent with _$Parent {
  factory Parent({
    required int id,
    required String name,
  }) = _Parent;

  factory Parent.fromJson(Map<String, dynamic> json) => _$ParentFromJson(json);
}
