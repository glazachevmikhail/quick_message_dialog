// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'quick_responses.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

QuickResponsesData _$QuickResponsesDataFromJson(Map<String, dynamic> json) {
  return _QuickResponsesData.fromJson(json);
}

/// @nodoc
mixin _$QuickResponsesData {
  int get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  int get sort => throw _privateConstructorUsedError;
  ListData get list => throw _privateConstructorUsedError;
  Parent? get parent => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $QuickResponsesDataCopyWith<QuickResponsesData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QuickResponsesDataCopyWith<$Res> {
  factory $QuickResponsesDataCopyWith(
          QuickResponsesData value, $Res Function(QuickResponsesData) then) =
      _$QuickResponsesDataCopyWithImpl<$Res, QuickResponsesData>;
  @useResult
  $Res call({int id, String name, int sort, ListData list, Parent? parent});

  $ListDataCopyWith<$Res> get list;
  $ParentCopyWith<$Res>? get parent;
}

/// @nodoc
class _$QuickResponsesDataCopyWithImpl<$Res, $Val extends QuickResponsesData>
    implements $QuickResponsesDataCopyWith<$Res> {
  _$QuickResponsesDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? sort = null,
    Object? list = null,
    Object? parent = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sort: null == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as int,
      list: null == list
          ? _value.list
          : list // ignore: cast_nullable_to_non_nullable
              as ListData,
      parent: freezed == parent
          ? _value.parent
          : parent // ignore: cast_nullable_to_non_nullable
              as Parent?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ListDataCopyWith<$Res> get list {
    return $ListDataCopyWith<$Res>(_value.list, (value) {
      return _then(_value.copyWith(list: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $ParentCopyWith<$Res>? get parent {
    if (_value.parent == null) {
      return null;
    }

    return $ParentCopyWith<$Res>(_value.parent!, (value) {
      return _then(_value.copyWith(parent: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_QuickResponsesDataCopyWith<$Res>
    implements $QuickResponsesDataCopyWith<$Res> {
  factory _$$_QuickResponsesDataCopyWith(_$_QuickResponsesData value,
          $Res Function(_$_QuickResponsesData) then) =
      __$$_QuickResponsesDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String name, int sort, ListData list, Parent? parent});

  @override
  $ListDataCopyWith<$Res> get list;
  @override
  $ParentCopyWith<$Res>? get parent;
}

/// @nodoc
class __$$_QuickResponsesDataCopyWithImpl<$Res>
    extends _$QuickResponsesDataCopyWithImpl<$Res, _$_QuickResponsesData>
    implements _$$_QuickResponsesDataCopyWith<$Res> {
  __$$_QuickResponsesDataCopyWithImpl(
      _$_QuickResponsesData _value, $Res Function(_$_QuickResponsesData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? sort = null,
    Object? list = null,
    Object? parent = freezed,
  }) {
    return _then(_$_QuickResponsesData(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sort: null == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as int,
      list: null == list
          ? _value.list
          : list // ignore: cast_nullable_to_non_nullable
              as ListData,
      parent: freezed == parent
          ? _value.parent
          : parent // ignore: cast_nullable_to_non_nullable
              as Parent?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_QuickResponsesData implements _QuickResponsesData {
  _$_QuickResponsesData(
      {required this.id,
      required this.name,
      required this.sort,
      required this.list,
      required this.parent});

  factory _$_QuickResponsesData.fromJson(Map<String, dynamic> json) =>
      _$$_QuickResponsesDataFromJson(json);

  @override
  final int id;
  @override
  final String name;
  @override
  final int sort;
  @override
  final ListData list;
  @override
  final Parent? parent;

  @override
  String toString() {
    return 'QuickResponsesData(id: $id, name: $name, sort: $sort, list: $list, parent: $parent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_QuickResponsesData &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.sort, sort) || other.sort == sort) &&
            (identical(other.list, list) || other.list == list) &&
            (identical(other.parent, parent) || other.parent == parent));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, name, sort, list, parent);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_QuickResponsesDataCopyWith<_$_QuickResponsesData> get copyWith =>
      __$$_QuickResponsesDataCopyWithImpl<_$_QuickResponsesData>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_QuickResponsesDataToJson(
      this,
    );
  }
}

abstract class _QuickResponsesData implements QuickResponsesData {
  factory _QuickResponsesData(
      {required final int id,
      required final String name,
      required final int sort,
      required final ListData list,
      required final Parent? parent}) = _$_QuickResponsesData;

  factory _QuickResponsesData.fromJson(Map<String, dynamic> json) =
      _$_QuickResponsesData.fromJson;

  @override
  int get id;
  @override
  String get name;
  @override
  int get sort;
  @override
  ListData get list;
  @override
  Parent? get parent;
  @override
  @JsonKey(ignore: true)
  _$$_QuickResponsesDataCopyWith<_$_QuickResponsesData> get copyWith =>
      throw _privateConstructorUsedError;
}

ListData _$ListDataFromJson(Map<String, dynamic> json) {
  return _ListData.fromJson(json);
}

/// @nodoc
mixin _$ListData {
  int get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ListDataCopyWith<ListData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ListDataCopyWith<$Res> {
  factory $ListDataCopyWith(ListData value, $Res Function(ListData) then) =
      _$ListDataCopyWithImpl<$Res, ListData>;
  @useResult
  $Res call({int id, String name});
}

/// @nodoc
class _$ListDataCopyWithImpl<$Res, $Val extends ListData>
    implements $ListDataCopyWith<$Res> {
  _$ListDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ListDataCopyWith<$Res> implements $ListDataCopyWith<$Res> {
  factory _$$_ListDataCopyWith(
          _$_ListData value, $Res Function(_$_ListData) then) =
      __$$_ListDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String name});
}

/// @nodoc
class __$$_ListDataCopyWithImpl<$Res>
    extends _$ListDataCopyWithImpl<$Res, _$_ListData>
    implements _$$_ListDataCopyWith<$Res> {
  __$$_ListDataCopyWithImpl(
      _$_ListData _value, $Res Function(_$_ListData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
  }) {
    return _then(_$_ListData(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ListData implements _ListData {
  _$_ListData({required this.id, required this.name});

  factory _$_ListData.fromJson(Map<String, dynamic> json) =>
      _$$_ListDataFromJson(json);

  @override
  final int id;
  @override
  final String name;

  @override
  String toString() {
    return 'ListData(id: $id, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ListData &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, name);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ListDataCopyWith<_$_ListData> get copyWith =>
      __$$_ListDataCopyWithImpl<_$_ListData>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ListDataToJson(
      this,
    );
  }
}

abstract class _ListData implements ListData {
  factory _ListData({required final int id, required final String name}) =
      _$_ListData;

  factory _ListData.fromJson(Map<String, dynamic> json) = _$_ListData.fromJson;

  @override
  int get id;
  @override
  String get name;
  @override
  @JsonKey(ignore: true)
  _$$_ListDataCopyWith<_$_ListData> get copyWith =>
      throw _privateConstructorUsedError;
}

Parent _$ParentFromJson(Map<String, dynamic> json) {
  return _Parent.fromJson(json);
}

/// @nodoc
mixin _$Parent {
  int get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ParentCopyWith<Parent> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ParentCopyWith<$Res> {
  factory $ParentCopyWith(Parent value, $Res Function(Parent) then) =
      _$ParentCopyWithImpl<$Res, Parent>;
  @useResult
  $Res call({int id, String name});
}

/// @nodoc
class _$ParentCopyWithImpl<$Res, $Val extends Parent>
    implements $ParentCopyWith<$Res> {
  _$ParentCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ParentCopyWith<$Res> implements $ParentCopyWith<$Res> {
  factory _$$_ParentCopyWith(_$_Parent value, $Res Function(_$_Parent) then) =
      __$$_ParentCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String name});
}

/// @nodoc
class __$$_ParentCopyWithImpl<$Res>
    extends _$ParentCopyWithImpl<$Res, _$_Parent>
    implements _$$_ParentCopyWith<$Res> {
  __$$_ParentCopyWithImpl(_$_Parent _value, $Res Function(_$_Parent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
  }) {
    return _then(_$_Parent(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Parent implements _Parent {
  _$_Parent({required this.id, required this.name});

  factory _$_Parent.fromJson(Map<String, dynamic> json) =>
      _$$_ParentFromJson(json);

  @override
  final int id;
  @override
  final String name;

  @override
  String toString() {
    return 'Parent(id: $id, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Parent &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, name);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ParentCopyWith<_$_Parent> get copyWith =>
      __$$_ParentCopyWithImpl<_$_Parent>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ParentToJson(
      this,
    );
  }
}

abstract class _Parent implements Parent {
  factory _Parent({required final int id, required final String name}) =
      _$_Parent;

  factory _Parent.fromJson(Map<String, dynamic> json) = _$_Parent.fromJson;

  @override
  int get id;
  @override
  String get name;
  @override
  @JsonKey(ignore: true)
  _$$_ParentCopyWith<_$_Parent> get copyWith =>
      throw _privateConstructorUsedError;
}
