// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quick_responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_QuickResponsesData _$$_QuickResponsesDataFromJson(
        Map<String, dynamic> json) =>
    _$_QuickResponsesData(
      id: json['id'] as int,
      name: json['name'] as String,
      sort: json['sort'] as int,
      list: ListData.fromJson(json['list'] as Map<String, dynamic>),
      parent: json['parent'] == null
          ? null
          : Parent.fromJson(json['parent'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_QuickResponsesDataToJson(
        _$_QuickResponsesData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'sort': instance.sort,
      'list': instance.list,
      'parent': instance.parent,
    };

_$_ListData _$$_ListDataFromJson(Map<String, dynamic> json) => _$_ListData(
      id: json['id'] as int,
      name: json['name'] as String,
    );

Map<String, dynamic> _$$_ListDataToJson(_$_ListData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

_$_Parent _$$_ParentFromJson(Map<String, dynamic> json) => _$_Parent(
      id: json['id'] as int,
      name: json['name'] as String,
    );

Map<String, dynamic> _$$_ParentToJson(_$_Parent instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
