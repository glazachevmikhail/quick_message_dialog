// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'quick_responses_get_item_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

QuickResponsesGetItemData _$QuickResponsesGetItemDataFromJson(
    Map<String, dynamic> json) {
  return _QuickResponsesGetItemData.fromJson(json);
}

/// @nodoc
mixin _$QuickResponsesGetItemData {
  int? get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  int? get sort => throw _privateConstructorUsedError;
  Parent? get parent => throw _privateConstructorUsedError;
  String? get text => throw _privateConstructorUsedError;
  String? get keywords => throw _privateConstructorUsedError;
  bool? get enableAI => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $QuickResponsesGetItemDataCopyWith<QuickResponsesGetItemData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QuickResponsesGetItemDataCopyWith<$Res> {
  factory $QuickResponsesGetItemDataCopyWith(QuickResponsesGetItemData value,
          $Res Function(QuickResponsesGetItemData) then) =
      _$QuickResponsesGetItemDataCopyWithImpl<$Res, QuickResponsesGetItemData>;
  @useResult
  $Res call(
      {int? id,
      String? name,
      int? sort,
      Parent? parent,
      String? text,
      String? keywords,
      bool? enableAI});

  $ParentCopyWith<$Res>? get parent;
}

/// @nodoc
class _$QuickResponsesGetItemDataCopyWithImpl<$Res,
        $Val extends QuickResponsesGetItemData>
    implements $QuickResponsesGetItemDataCopyWith<$Res> {
  _$QuickResponsesGetItemDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? sort = freezed,
    Object? parent = freezed,
    Object? text = freezed,
    Object? keywords = freezed,
    Object? enableAI = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      sort: freezed == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as int?,
      parent: freezed == parent
          ? _value.parent
          : parent // ignore: cast_nullable_to_non_nullable
              as Parent?,
      text: freezed == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String?,
      keywords: freezed == keywords
          ? _value.keywords
          : keywords // ignore: cast_nullable_to_non_nullable
              as String?,
      enableAI: freezed == enableAI
          ? _value.enableAI
          : enableAI // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ParentCopyWith<$Res>? get parent {
    if (_value.parent == null) {
      return null;
    }

    return $ParentCopyWith<$Res>(_value.parent!, (value) {
      return _then(_value.copyWith(parent: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_QuickResponsesGetItemDataCopyWith<$Res>
    implements $QuickResponsesGetItemDataCopyWith<$Res> {
  factory _$$_QuickResponsesGetItemDataCopyWith(
          _$_QuickResponsesGetItemData value,
          $Res Function(_$_QuickResponsesGetItemData) then) =
      __$$_QuickResponsesGetItemDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      String? name,
      int? sort,
      Parent? parent,
      String? text,
      String? keywords,
      bool? enableAI});

  @override
  $ParentCopyWith<$Res>? get parent;
}

/// @nodoc
class __$$_QuickResponsesGetItemDataCopyWithImpl<$Res>
    extends _$QuickResponsesGetItemDataCopyWithImpl<$Res,
        _$_QuickResponsesGetItemData>
    implements _$$_QuickResponsesGetItemDataCopyWith<$Res> {
  __$$_QuickResponsesGetItemDataCopyWithImpl(
      _$_QuickResponsesGetItemData _value,
      $Res Function(_$_QuickResponsesGetItemData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? sort = freezed,
    Object? parent = freezed,
    Object? text = freezed,
    Object? keywords = freezed,
    Object? enableAI = freezed,
  }) {
    return _then(_$_QuickResponsesGetItemData(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      sort: freezed == sort
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as int?,
      parent: freezed == parent
          ? _value.parent
          : parent // ignore: cast_nullable_to_non_nullable
              as Parent?,
      text: freezed == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String?,
      keywords: freezed == keywords
          ? _value.keywords
          : keywords // ignore: cast_nullable_to_non_nullable
              as String?,
      enableAI: freezed == enableAI
          ? _value.enableAI
          : enableAI // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_QuickResponsesGetItemData implements _QuickResponsesGetItemData {
  _$_QuickResponsesGetItemData(
      {required this.id,
      required this.name,
      required this.sort,
      required this.parent,
      required this.text,
      required this.keywords,
      required this.enableAI});

  factory _$_QuickResponsesGetItemData.fromJson(Map<String, dynamic> json) =>
      _$$_QuickResponsesGetItemDataFromJson(json);

  @override
  final int? id;
  @override
  final String? name;
  @override
  final int? sort;
  @override
  final Parent? parent;
  @override
  final String? text;
  @override
  final String? keywords;
  @override
  final bool? enableAI;

  @override
  String toString() {
    return 'QuickResponsesGetItemData(id: $id, name: $name, sort: $sort, parent: $parent, text: $text, keywords: $keywords, enableAI: $enableAI)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_QuickResponsesGetItemData &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.sort, sort) || other.sort == sort) &&
            (identical(other.parent, parent) || other.parent == parent) &&
            (identical(other.text, text) || other.text == text) &&
            (identical(other.keywords, keywords) ||
                other.keywords == keywords) &&
            (identical(other.enableAI, enableAI) ||
                other.enableAI == enableAI));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, id, name, sort, parent, text, keywords, enableAI);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_QuickResponsesGetItemDataCopyWith<_$_QuickResponsesGetItemData>
      get copyWith => __$$_QuickResponsesGetItemDataCopyWithImpl<
          _$_QuickResponsesGetItemData>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_QuickResponsesGetItemDataToJson(
      this,
    );
  }
}

abstract class _QuickResponsesGetItemData implements QuickResponsesGetItemData {
  factory _QuickResponsesGetItemData(
      {required final int? id,
      required final String? name,
      required final int? sort,
      required final Parent? parent,
      required final String? text,
      required final String? keywords,
      required final bool? enableAI}) = _$_QuickResponsesGetItemData;

  factory _QuickResponsesGetItemData.fromJson(Map<String, dynamic> json) =
      _$_QuickResponsesGetItemData.fromJson;

  @override
  int? get id;
  @override
  String? get name;
  @override
  int? get sort;
  @override
  Parent? get parent;
  @override
  String? get text;
  @override
  String? get keywords;
  @override
  bool? get enableAI;
  @override
  @JsonKey(ignore: true)
  _$$_QuickResponsesGetItemDataCopyWith<_$_QuickResponsesGetItemData>
      get copyWith => throw _privateConstructorUsedError;
}
