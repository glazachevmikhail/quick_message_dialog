// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quick_responses_get_item_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_QuickResponsesGetItemData _$$_QuickResponsesGetItemDataFromJson(
        Map<String, dynamic> json) =>
    _$_QuickResponsesGetItemData(
      id: json['id'] as int?,
      name: json['name'] as String?,
      sort: json['sort'] as int?,
      parent: json['parent'] == null
          ? null
          : Parent.fromJson(json['parent'] as Map<String, dynamic>),
      text: json['text'] as String?,
      keywords: json['keywords'] as String?,
      enableAI: json['enableAI'] as bool?,
    );

Map<String, dynamic> _$$_QuickResponsesGetItemDataToJson(
        _$_QuickResponsesGetItemData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'sort': instance.sort,
      'parent': instance.parent,
      'text': instance.text,
      'keywords': instance.keywords,
      'enableAI': instance.enableAI,
    };
