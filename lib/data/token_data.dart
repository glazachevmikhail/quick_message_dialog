import 'package:shared_preferences/shared_preferences.dart';

void token(int date) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();

  String? authToken = prefs.getString('auth_token');

  if (authToken != null) {
    bool isTokenExpired = checkTokenExpiration(date);
    if (isTokenExpired) {
      await prefs.remove('auth_token');
      await prefs.remove('date');
    }
  }
}

bool checkTokenExpiration(int date) {
  DateTime now = DateTime.now();

  DateTime expirationDate = DateTime.fromMillisecondsSinceEpoch(date * 1000);

  if (now.isAfter(expirationDate)) {
    return true;
  } else {
    return false;
  }
}
