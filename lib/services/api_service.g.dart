// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps,no_leading_underscores_for_local_identifiers

class _ApiService implements ApiService {
  _ApiService(
    this._dio, {
    this.baseUrl,
  }) {
    baseUrl ??= 'https://api.chatapp.online/v1';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ResponseData<AuthResponseData>> login(authRequest) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{r'Content-Type': 'application/json'};
    _headers.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    _data.addAll(authRequest.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ResponseData<AuthResponseData>>(Options(
      method: 'POST',
      headers: _headers,
      extra: _extra,
      contentType: 'application/json',
    )
            .compose(
              _dio.options,
              '/tokens',
              queryParameters: queryParameters,
              data: _data,
            )
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResponseData<AuthResponseData>.fromJson(
      _result.data!,
      (json) => AuthResponseData.fromJson(json as Map<String, dynamic>),
    );
    return value;
  }

  @override
  Future<HttpResponse<ResponseData<List<QuickResponsesData>>>>
      getQuickResponses({
    contentType = "application/json",
    required token,
    required query,
  }) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'query': query};
    final _headers = <String, dynamic>{
      r'Content-Type': contentType,
      r'Authorization': token,
    };
    _headers.removeWhere((k, v) => v == null);
    final Map<String, dynamic>? _data = null;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<HttpResponse<ResponseData<List<QuickResponsesData>>>>(
            Options(
      method: 'GET',
      headers: _headers,
      extra: _extra,
      contentType: contentType,
    )
                .compose(
                  _dio.options,
                  '/quickResponseLists/quickResponses/search',
                  queryParameters: queryParameters,
                  data: _data,
                )
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResponseData<List<QuickResponsesData>>.fromJson(
      _result.data!,
      (json) => (json as List<dynamic>)
          .map<QuickResponsesData>(
              (i) => QuickResponsesData.fromJson(i as Map<String, dynamic>))
          .toList(),
    );
    final httpResponse = HttpResponse(value, _result);
    return httpResponse;
  }

  @override
  Future<HttpResponse<ResponseData<QuickResponsesGetItemData>>> getItemQR({
    contentType = "application/json",
    required token,
    required listId,
    required responseId,
  }) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{
      r'Content-Type': contentType,
      r'Authorization': token,
    };
    _headers.removeWhere((k, v) => v == null);
    final Map<String, dynamic>? _data = null;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<HttpResponse<ResponseData<QuickResponsesGetItemData>>>(
            Options(
      method: 'GET',
      headers: _headers,
      extra: _extra,
      contentType: contentType,
    )
                .compose(
                  _dio.options,
                  '/quickResponseLists/${listId}/quickResponses/${responseId}',
                  queryParameters: queryParameters,
                  data: _data,
                )
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResponseData<QuickResponsesGetItemData>.fromJson(
      _result.data!,
      (json) =>
          QuickResponsesGetItemData.fromJson(json as Map<String, dynamic>),
    );
    final httpResponse = HttpResponse(value, _result);
    return httpResponse;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
