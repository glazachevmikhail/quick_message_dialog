import 'package:quick_message_dialog/models/quick_responses/quick_responses.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart' hide Headers;

import '../models/auth/auth_request.dart';
import '../models/auth/auth_response.dart';
import '../models/quick_responses/quick_responses_get_item_data.dart';

part 'api_service.g.dart';

@RestApi(baseUrl: "https://api.chatapp.online/v1")
abstract class ApiService {
  factory ApiService(Dio dio) => _ApiService(dio);
//[AUTH]
  @POST("/tokens")
  @Headers({'Content-Type': 'application/json'})
  Future<ResponseData<AuthResponseData>> login(
    @Body() AuthRequest authRequest,
  );

  //[QUICK_RESPONSES_SEARCH_ALL]
  @GET('/quickResponseLists/quickResponses/search')
  Future<HttpResponse<ResponseData<List<QuickResponsesData>>>>
      getQuickResponses({
    @Header("Content-Type") String contentType = "application/json",
    @Header("Authorization") required String token,
    @Query("query") required String query,
  });

//[QUICK_RESPONSES_GET_ITEM]
  @GET('/quickResponseLists/{listId}/quickResponses/{responseId}')
  Future<HttpResponse<ResponseData<QuickResponsesGetItemData>>> getItemQR({
    @Header("Content-Type") String contentType = "application/json",
    @Header("Authorization") required String token,
    @Path("listId") required int listId,
    @Path("responseId") required int responseId,
  });
}
