import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quick_message_dialog/bloc/quick_responses/quick_responses_bloc.dart';
import 'package:quick_message_dialog/bloc/quick_responses/quick_responses_event.dart';
import 'package:quick_message_dialog/widgets/quick_responses/widgets/quick_responses_state_widget.dart';
import '../auth_screen/widgets/auth_textfield.dart';

class QuickResponsesScreenWidget extends StatelessWidget {
  const QuickResponsesScreenWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final model = context.read<QuickResponsesBloc>();
    return Material(
      color: Colors.transparent,
      child: Builder(
        builder: (context) => Column(
          children: [
            AuthTextFieldWidget(
              text: 'поиск..',
              height: 30,
              suffixIcon: GestureDetector(
                onTap: () {},
                child: const Icon(
                  Icons.search,
                  color: Color.fromRGBO(0, 0, 0, 0.24),
                ),
              ),
              onChanged: (v) {
                model.query = v;
                v.isEmpty
                    ? model.add(QuickResponsesSearchQueryEmptyEvent())
                    : model.add(QuickResponsesSearchEvent());
              },
            ),
            const SizedBox(
              height: 20,
            ),
            const QuickResponsesStateWidget(),
            const SizedBox(height: 15),
          ],
        ),
      ),
    );
  }
}
