import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quick_message_dialog/models/quick_responses/quick_responses_get_item_data.dart';
import '../../../bloc/quick_responses/quick_responses_bloc.dart';
import '../../../bloc/quick_responses/quick_responses_state.dart';

class QuickResponsesStateWidget extends StatelessWidget {
  const QuickResponsesStateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuickResponsesBloc, QuickResponsesState>(
        builder: (context, state) {
      if (state is QuickResponsesEmptyState) {
        return const SizedBox.shrink();
      }

      if (state is QuickResponsesLoadingState) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }

      if (state is QuickResponsesErrorState) {
        return const Text('ERROR');
      }
      if (state is QuickResponsesLoadedState) {
        return Expanded(
          child: ListView.builder(
            itemCount: state.quickResponsesList.length ?? 0,
            itemBuilder: (context, index) {
              final list = state.quickResponsesList.toList();
              list.sort((a, b) => a.id.compareTo(b.id));
              final quickResponse = list[index];
              final name = quickResponse.name;
              final id = quickResponse.id;
              final parent = quickResponse.parent;
              final parentName = parent?.name ?? '';
              final bool isLastItem = index == list.length - 1;

              return Container(
                padding: isLastItem ? const EdgeInsets.only(bottom: 10) : null,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255, 255, 255, 0.9),
                  borderRadius: isLastItem
                      ? const BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0),
                        )
                      : null,
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 9.0, left: 18),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      IdQuickResponsesWidget(id: id),
                      const SizedBox(width: 15),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            NameQuickResponsesWidget(name: name),
                            const SizedBox(height: 6),
                            parent != null
                                ? ParrentQuickResponsesWidget(
                                    parrentName: parentName,
                                  )
                                : const SizedBox.shrink(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        );
      }
      return const SizedBox.shrink();
    });
  }
}

class IdQuickResponsesWidget extends StatelessWidget {
  final int? id;
  const IdQuickResponsesWidget({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 40,
      child: Text(
        '$id',
        style: const TextStyle(fontSize: 14.0),
        softWrap: true,
        maxLines: 20,
      ),
    );
  }
}

class NameQuickResponsesWidget extends StatelessWidget {
  final String name;
  const NameQuickResponsesWidget({Key? key, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      name,
      style: const TextStyle(fontSize: 14.0),
      softWrap: true,
      maxLines: 20,
    );
  }
}

class ParrentQuickResponsesWidget extends StatelessWidget {
  final String parrentName;
  const ParrentQuickResponsesWidget({Key? key, required this.parrentName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      'Мессенджеры > $parrentName',
      style:
          const TextStyle(fontSize: 10.0, color: Color.fromRGBO(0, 0, 0, 0.6)),
      softWrap: true,
      maxLines: 20,
    );
  }
}
