import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/auth/auth_bloc.dart';
import '../../../bloc/auth/auth_state.dart';

class BlocStateWidget extends StatelessWidget {
  const BlocStateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        final navigator = Navigator.of(context);
        if (state is AuthEmptyState) {
          return const SizedBox.shrink();
        }
        if (state is AuthLoadingState) {
          return const Center(child: CircularProgressIndicator());
        }

        if (state is AuthLoadedState) {
          if (state.authResponseData != null) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              context.read<AuthBloc>().showQuickResponsesScreen(context);
            });
          } else {
            return const Text(
              'Неверный логин или пароль',
              style: TextStyle(color: Colors.redAccent),
            );
          }
        }
        if (state is AuthErrorState) {
          return Text('ERROR');
        }
        return const SizedBox.shrink();
      },
    );
  }
}
