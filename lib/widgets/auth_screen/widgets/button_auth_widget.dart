import 'package:flutter/material.dart';

class ButtonAuthWidget extends StatelessWidget {
  final VoidCallback onPressed;
  final String buttonText;
  final Color buttonColor;
  final Color textColor;
  final double? radius;
  final double? height;
  final double? width;
  final double? splashColor;
  final TextStyle? textStyle;

  ButtonAuthWidget(
      {required this.onPressed,
      required this.buttonText,
      required this.buttonColor,
      this.textColor = Colors.black,
      this.radius,
      this.splashColor,
      this.height,
      this.width,
      this.textStyle});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius ?? 0),
      child: Material(
        color: buttonColor,
        child: InkWell(
          onTap: onPressed,
          splashColor:
              Colors.red.withOpacity(splashColor ?? 0), // Цвет эффекта Ripple
          borderRadius: BorderRadius.circular(8.0), // Задаем радиус границы
          child: Container(
            height: height,
            width: width,
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
            decoration: const BoxDecoration(
              color: Colors.transparent,
            ),
            child: Center(
              child: Text(
                buttonText,
                style: textStyle ??
                    TextStyle(
                      color: textColor,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
