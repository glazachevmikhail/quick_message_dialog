import 'package:flutter/material.dart';

class AuthTextFieldWidget extends StatelessWidget {
  final Widget? suffixIcon;
  final double height;
  final double width;
  final VoidCallback? onEditingComplete;
  final Color? color;
  final String? text;
  final TextEditingController? controller;
  final ValueChanged<String>? onChanged;
  const AuthTextFieldWidget({
    Key? key,
    this.onEditingComplete,
    this.text,
    this.controller,
    this.color = Colors.white,
    this.onChanged,
    this.height = 10.0,
    this.width = 16.0,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      controller: controller,
      textInputAction: TextInputAction.next,
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        suffixIcon: suffixIcon,
        filled: true,
        fillColor: color,
        contentPadding: EdgeInsets.symmetric(vertical: height, horizontal: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.black),
          borderRadius: BorderRadius.circular(15),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.black),
          borderRadius: BorderRadius.circular(15),
        ),
        hintText: text,
        hintStyle: const TextStyle(color: Colors.grey),
      ),
      onEditingComplete: onEditingComplete,
    );
  }
}
