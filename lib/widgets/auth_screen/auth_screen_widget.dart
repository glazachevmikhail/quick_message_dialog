import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quick_message_dialog/bloc/auth/auth_bloc.dart';
import 'package:quick_message_dialog/bloc/auth/auth_event.dart';
import 'package:quick_message_dialog/data/token_data.dart';
import 'package:quick_message_dialog/widgets/auth_screen/widgets/auth_textfield.dart';
import 'package:quick_message_dialog/widgets/auth_screen/widgets/bloc_state_widget.dart';
import 'package:quick_message_dialog/widgets/auth_screen/widgets/button_auth_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthScreenWidget extends StatefulWidget {
  const AuthScreenWidget({Key? key}) : super(key: key);

  @override
  State<AuthScreenWidget> createState() => _AuthScreenWidgetState();
}

class _AuthScreenWidgetState extends State<AuthScreenWidget> {
  @override
  void didChangeDependencies() async {
    final prefs = await SharedPreferences.getInstance();
    final date = prefs.getInt('date') ?? 0;
    token(date);
    if (prefs.getString('auth_token') != null) {
      context.read<AuthBloc>().showQuickResponsesScreen(context);
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final model = context.read<AuthBloc>();
    model.emailController.text = "support@chatapp.online";
    model.passwordController.text = "NwPss789342";
    return Material(
      color: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AuthTextFieldWidget(
              text: 'введите email..',
              controller: model.emailController,
            ),
            const SizedBox(height: 15),
            AuthTextFieldWidget(
              text: 'введите пароль..',
              controller: model.passwordController,
              onEditingComplete: () => model.add(AuthLoadEvent()),
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: BlocStateWidget(),
            ),
            const SizedBox(height: 10),
            ButtonAuthWidget(
              onPressed: () => model.add(AuthLoadEvent()),
              buttonText: 'войти',
              buttonColor: Colors.white,
              radius: 15,
            ),
          ],
        ),
      ),
    );
  }
}
